# Planner4U Home

Цель данного проекта - разработка приложения для отслеживания и работы над задачами. Добавляйте задачи. Выполняйте их! Отмечайте свои успехи! Не забывайте о важных датах. Создавайте условные задачи, а также регулярные планы. Объеденяйте дела в группы, а также создавайте подзадачи. Работайте над совместными делами вместе с друзьяи или коллегами. 

### Terminal application

Программа предоставляет удобный и локаничный консольный интерфейс для работы с вышеперечисленными функциями. 

**Как установить программу:**

Скопируйте репоизторий:

```bash
user$ git clone git@bitbucket.org:AlexKuchinsky/planner4u.git
```
Перейдите в дирикторию Desktop и установите программу:

```bash
user$ cd Desktop
user$ python3 setup.py install
```
**_Важно_! ** Перед первым использованием, запустите инициалирующую команду, указав connection string вашей базы данных (_MySQL, PostgreSQL, Oracle, Microsoft SQL Server, SQLite_) и имя пользователя. Обращение к программе осуществляется с помощю команды **_pu_**.

```bash
user$ pu init --connection-string="db://USERNAME:PASSWORD@HOST:PORT/DB_NAME" 
--username="sergio"
Database was set up
User was added: sergio (id 1)
```
Далее, разместите файл конфигураций planner4uconfig.json в вашей домашней директории.

``` json
{
    // Your user id from database or just username
	"user id": 1,
	"connection string" : "db://USERNAME:PASSWORD@HOST:PORT/DB_NAME",
    // Loggin details
	"log output": {
		"file":{
			"level": "debug",
			"file path": "file/path/to/write/log"
		},
		"terminal":{
			"level": "critical"	
		}
	}
	
}
```

Наши поздравления! Теперь вы можете запустить программу, используя команду **_pu_**!

**Пример использования**

``` bash
user$ pu task add -t "Have a good day"
------ Task was created: 
  Have a good day
 Task id: 6758
 Type: simple
 Description: -
 Created on: 2018-06-07
 Priority: regular
 Status: created
 Close on last subtask execution: no
 Priority on overdue: regular
 
user$ pu tasks
------ Creator: 
 3 - Prepare for english exam (simple, executed)
 14 - Perpare documents for Serbian embassy (simple, work)
 6758 - Have a good day (simple, created)
 
user$ pu task update -i 6758 --status overdue
------ Task was updated:
  Have a good day
 Task id: 6758
 Type: simple
 Description: -
 Created on: 2018-06-07
 Priority: regular
 Status: overdue
 Close on last subtask execution: no
 Priority on overdue: regular
```



### Library 

Билиблиотека расположена в директории Library данного репозитория и представляет собой API для работы с приложением.  

**Как установить бибилиотеку:**

Скопируйте репоизторий:

``` bash 
user$ git clone git@bitbucket.org:AlexKuchinsky/planner4u.git
```
Перейдите в дирикторию Library и установите модуль библиотеки:

``` bash
user$ cd Library
user$ python3 setup.py install
```
Вы также можете запустить тесты бибилотеки:

``` bash
user$ python3 setup.py test
```
**Как использовать библиотеку:**

1. Импортируйте модуль библиотеки
2. Импортируйте из пакета _services_ одноименный модуль.
3. Пакет _storage_ содержит модули реализации DAL для различных типов хранилищ, выберете подходящий вам модуль и импортируйте его
4. Используя импортированный модуль, создайте _data storage class_ 
5. Имопртируйте из модуля services класс _Service_
6. Создайте объект класса Service, предоставив _data storage class_ 

**Пример использования библиотеки:**

``` python
>>> import planner4ucore
>>> from planner4ucore.services import services
>>> from planner4ucore.storage import sqlalchemy_storage
>>> connection_string = "mysql://USERNAME:PASSWORD@HOST:PORT/DB_NAME?charset=utf8"
>>> data_storage = sqlalchemy_storage.SQLAlchemyStorage(connection_string)
>>> service = services.Service(data_storage)
>>> from planner4ucore.models import models as mo
>>> user = service.add_user(mo.User("sergio"))
>>> task = service.add_task(mo.Task(user.user_id, "Eat, pray, love, code"))
>>> service.get_user_tasks(user.user_id)[0].title
'Eat, pray, love, code'
```

### Contacts

Если вы чувствуете в себе силы поддержать меня морально, материально или же просто предложить новые идеи для проекта, пишите: alexkuchinskydev@gmail.com

