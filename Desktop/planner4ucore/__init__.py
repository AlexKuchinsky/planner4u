"""
    This core package offers all necessary classes and functions to work with
    PlannerU4 task-tracker library.

    Packages:
        models - package contains core models used in library
        services - package provides interface for data storage access
        storage - package provides access to storage itself

    How to user this library (version 2.3):
        - In services package find services module
        - Import Service class from this module
        - Import data storage class from one of the modules in storage package
        - Create object of Service class providing data storage instance
        - Use service instance to satisfy your requirements

    Example:
        >>> import planner4ucore
        >>> from planner4ucore.services import services
        >>> from planner4ucore.storage import sqlalchemy_storage
        >>> connection_string = "mysql://USERNAME:PASSWORD@HOST:PORT/DB_NAME?charset=utf8"
        >>> data_storage = sqlalchemy_storage.SQLAlchemyStorage(connection_string)
        >>> service = services.Service(data_storage)
        >>> from planner4ucore.models import models as mo
        >>> user = service.add_user(mo.User("sergio"))
        >>> task = service.add_task(mo.Task(user.user_id, "Eat, pray, love, code"))
        >>> service.get_user_tasks(user.user_id)[0].title
        'Eat, pray, love, code'

"""