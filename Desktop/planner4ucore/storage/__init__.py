"""
    This package contains data storage logic and related modules

    Modules:
        sqlalchemy_storage - implements data access using SQLAlchemy ORM

    How to use it:
        >>> import planner4ucore
        >>> from planner4ucore.services import services
        >>> from planner4ucore.storage import sqlalchemy_storage
        >>> connection_string = "mysql://USERNAME:PASSWORD@HOST:PORT/DB_NAME?charset=utf8"
        >>> data_storage = sqlalchemy_storage.SQLAlchemyStorage(connection_string)
        >>> service = services.Service(data_storage)
        >>> from planner4ucore.models import models as mo
        >>> user = service.add_user(mo.User("sergio"))
        >>> task = service.add_task(mo.Task(user.user_id, "Eat, pray, love, code"))
        >>> service.get_user_tasks(user.user_id)[0].title
        'Eat, pray, love, code'

"""
