"""
    Contains dictionaries where "key" is terminal argument name
    and "value" is function which shows how this value
    has to be stored in model

    Attributes:
        CONDITION_TASK_RELATIONS_ATTRIBUTES_TRANSFORMER (dict)
        GROUP_ATTRIBUTES_TRANSFORMER (dict)
        NOTIFICATION_ATTRIBUTES_TRANSFORMER (dict)
        PLAN_ATTRIBUTES_TRANSFORMER (dict)
        REMINDER_ATTRIBUTES_TRANSFORMER(dict)
        SUBTASK_RELATION_ATTRIBUTES_TRANSFORMER (dict)
        TASK_DATE_ATTRIBUTES_TRANSFORMER (dict)
        TASK_GROUP_ATTRIBUTES_TRANSFORMER (dict)
        TASK_ATTRIBUTES_TRANSFORMER(dict)
        USER_EXECUTOR_RELATION_ATTRIBUTES_TRANSFORMER (dict)

"""
import planner4ucore.models.enums as me
import planner4ucore.models.models as mo


def get_attributes(arguments_to_attributes, attributes_transformer, arguments, filter_none=False):
    """
        Constructs and returns dictionary for model
        based on parser arguments which are translated into
        suitable model attributes names (arguments_to_attributes) and
        transformed in suitable form (attributes_transformer)
        Doesn't include None values if filter_none is True

        Args:
           arguments_to_attributes (dict) how to translate arguments into model attributes
           attributes_transformer (dict): how to handle arguments
           arguments (dict): dicitonary of arguments to be handled
           filter_none (bool): whether to consider None values in result models dictionary

        Returns:
             Dictionary for model attributes

    """
    model_attributes = {}
    for argument, value in arguments.items():
        if (filter_none and value is None) or argument not in arguments_to_attributes:
            continue
        model_attribute = arguments_to_attributes[argument]
        model_attributes[model_attribute] = attributes_transformer[model_attribute](value)

    return model_attributes


CONDITION_TASK_RELATIONS_ATTRIBUTES_TRANSFORMER = {
    mo.CONDITION_TASK_RELATION_ATTR_TRIGGER_ID: lambda task_id: task_id,
    mo.CONDITION_TASK_RELATION_ATTR_STATUS: lambda status: me.STRING_TO_TASK_STATUS_ENUM[status]
}

GROUP_ATTRIBUTES_TRANSFORMER = {
    mo.GROUP_ATTR_TITLE: lambda title: title
}

NOTIFICATION_ATTRIBUTES_TRANSFORMER = {
    mo.NOTIFICATION_ATTR_DATE_ID: lambda date_id: date_id,
    mo.NOTIFICATION_ATTR_DAYS: lambda days: days,
    mo.NOTIFICATION_ATTR_TIME: lambda time: time,
    mo.NOTIFICATION_ATTR_USED: lambda used: me.STRING_TO_ACTIVE_ENUM[used]
}

PLAN_ATTRIBUTES_TRANSFORMER = {
    mo.PLAN_ATTR_DATE: lambda start_date: start_date,
    mo.PLAN_ATTR_TYPE: lambda repeat_type: me.STRING_TO_REPETITION_TYPE_ENUM[repeat_type],
    mo.PLAN_ATTR_INTERVAL: lambda interval: interval,
    mo.PLAN_ATTR_END_TYPE: lambda end_type: me.STRING_TO_PLAN_COMPLETION_CRITERIA_ENUM[end_type],
    mo.PLAN_ATTR_END_DATE: lambda end_date: end_date,
    mo.PLAN_ATTR_REPETITIONS_NUMBER: lambda repetitions_number: repetitions_number,
    mo.PLAN_ATTR_ACTIVE: lambda active: me.STRING_TO_ACTIVE_ENUM[active],
    mo.PLAN_ATTR_TASK_ID: lambda task_id: task_id
}

REMINDER_ATTRIBUTES_TRANSFORMER = {
    mo.REMINDER_ATTR_TASK: lambda task_id: task_id,
    mo.REMINDER_ATTR_DATE: lambda raise_date: raise_date,
    mo.REMINDER_ATTR_TIME: lambda time: time,
    mo.REMINDER_ATTR_INTERVAL: lambda interval: interval,
    mo.REMINDER_ATTR_ACTIVE: lambda active: me.STRING_TO_ACTIVE_ENUM[active]
}

SUBTASK_RELATION_ATTRIBUTES_TRANSFORMER = {
    mo.SUBTASK_RELATION_ATTR_PARENT_ID: lambda parent_task_id: parent_task_id,
    mo.SUBTASK_RELATION_ATTR_CHILD_ID: lambda sub_task_id: sub_task_id,
    mo.SUBTASK_RELATION_ATTR_TYPE: lambda relation_type: me.STRING_TO_TASK_RELATION_ENUM[relation_type]
}

TASK_DATE_ATTRIBUTES_TRANSFORMER = {
    mo.DATE_ATTR_TASK_ID: lambda task_id: task_id,
    mo.DATE_ATTR_TYPE: lambda date_type: me.STRING_TO_DATE_TYPE_ENUM[date_type],
    mo.DATE_ATTR_DATE: lambda date: date
}

TASK_GROUP_ATTRIBUTES_TRANSFORMER = {
    mo.GROUP_RELATION_ATTR_TASK_ID: lambda task_id: task_id,
    mo.GROUP_RELATION_ATTR_GROUP_ID: lambda group_id: group_id
}

TASK_ATTRIBUTES_TRANSFORMER = {
    mo.TASK_ATTR_TITLE: lambda title: title,
    mo.TASK_ATTR_DESCRIPTION: lambda description: description,
    mo.TASK_ATTR_PRIORITY: lambda priority: me.STRING_TO_TASK_PRIORITY_ENUM[priority],
    mo.TASK_ATTR_DEPENDENT: lambda sub_dependent: me.STRING_TO_SUBTASK_DEPENDENCY_ENUM[sub_dependent],
    mo.TASK_ATTR_STATUS: lambda status: me.STRING_TO_TASK_STATUS_ENUM[status],
    mo.TASK_ATTR_OVERDUE_PRIORITY: lambda priority: me.STRING_TO_TASK_PRIORITY_ENUM[priority] if priority else None
}

USER_EXECUTOR_RELATION_ATTRIBUTES_TRANSFORMER = {
    mo.EXECUTOR_RELATION_ATTR_EXECUTOR_ID: lambda user_id: user_id,
    mo.EXECUTOR_RELATION_ATTR_TASK_ID: lambda task_id: task_id
}
