"""
    Contains entry point for Planner4UDesktop, assigned in setup.py

    Functions:
        main - entry point

"""
import os

from planner4ucore.services.services import Service
from planner4ucore.storage.sqlalchemy_storage import SQLAlchemyStorage
from planner4ucore.services.configurations import JSONConfigurator
from planner4ucore.models import models as mo

from terminal import initializations
from terminal.handlers import TerminalHandler
from terminal.handlers import error_catcher


@error_catcher
def main():
    """
        Entry point for application

        Step by step does:
            1) Parses arguments
            2) Performs initial set up
            3) Reads configuration file
            4) Sets up data storage object
            5) Sets up service object
            6) Checks user existence
            7) Updates system state
            8) Invokes arguments handler

    """
    # Initialize parser and get namespace
    namespace = initializations.get_parsed_namespace()

    # Execute initial set up, if needed
    initial_set_up = getattr(namespace, initializations.FIRST_COMMAND) == initializations.INIT_COMMAND
    if initial_set_up:
        TerminalHandler.handle_init_command(Service, SQLAlchemyStorage, namespace)
        return

    # Read configuration file
    try:
        config_path = os.path.join(os.getenv("HOME"), JSONConfigurator.DEFAULT_FILE_NAME)
        configuration = JSONConfigurator(config_path)
    except Exception as e:
        print("ERROR:", str(e))
        print("Please, make sure you have placed palnner4Uconfig.json file in home dir or specified "
              "your path using --config-path command. \n Example of configuration file: ")
        print(JSONConfigurator.CONFIGURATION_FILE_EXAMPLE)
        return

    # Setup data storage
    data_storage = SQLAlchemyStorage(connection_string=configuration.connection_string)

    # Setup handler for arguments and handle commands
    service = Service(data_storage)

    # Check user
    user = service.get_user(user_id=configuration.user_id)
    if user is None:
        print("Can't find user wiht id {user_id}".format(user_id=configuration.user_id))
        return

    # Assign user
    namespace.user_id = user.user_id

    # Updating state
    service.update_system_state()

    # Handling arguments
    menu = TerminalHandler(service)
    menu.handle(namespace)


if __name__ == '__main__':
    main()
