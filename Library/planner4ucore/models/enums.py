"""
    Contains enums for models, and dictionaries for
    representation of this enums

    Classes:
        TaskStatus
        TaskType
        TaskPriority
        TaskRelation
        GroupType
        DateType
        RepetitionType
        PlanCompletionCriteria
        PlanState
        RelationConfirmation
        SubtaskDependency
        Used
        Active

    Global variables:
        # string representations of enums
        STATUS_DEFINED = "defined"
        STATUS_CREATED = "created"
        STATUS_WORK = "work"
        STATUS_EXECUTED = "executed"
        STATUS_OVERDUE = "overdue"
        STATUS_CLOSED = "closed"
        STATUS_PAUSED = "paused"

        TASK_TYPE_SIMPLE = "simple"
        TASK_TYPE_CONDITION = "condition"
        TASK_TYPE_PLAN = "plan"

        PRIORITY_MINOR = "minor"
        PRIORITY_LOW = "low"
        PRIORITY_REGULAR = "regular"
        PRIORITY_HIGH = "high"
        PRIORITY_CRITICAL = "critical"

        SUBTASK_BLOCKING = "blocking"
        SUBTASK_DEPENDS = "depends"
        SUBTASK_CONNECTED = "connected"

        DATE_TYPE_START = "start"
        DATE_TYPE_END = "end"
        DATE_TYPE_EVENT = "event"

        NO = "no"
        YES = "yes"

        PLAN_REPETITION_DAILY = "daily"
        PLAN_REPETITION_WEEKLY = "weekly"
        PLAN_REPETITION_MONTHLY = "monthly"
        PLAN_REPETITION_ANNUALLY = "annually"

        PLAN_ACTIVE_STATE = "active"
        PLAN_INACTIVE_STATE = "inactive"

        PLAN_END_NEVER = "never"
        PLAN_END_DATE = "date"
        PLAN_END_NUMBER = "number"

        # Following dictionaries transform string to enum value
        STRING_TO_TASK_STATUS_ENUM
        STRING_TO_TASK_TYPE_ENUM
        STRING_TO_TASK_PRIORITY_ENUM
        STRING_TO_TASK_RELATION_ENUM
        STRING_TO_DATE_TYPE_ENUM
        STRING_TO_REPETITION_TYPE_ENUM
        STRING_TO_PLAN_COMPLETION_CRITERIA_ENUM
        STRING_TO_PLAN_STATE_ENUM
        STRING_TO_SUBTASK_DEPENDENCY_ENUM
        STRING_TO_USED_ENUM
        STRING_TO_ACTIVE_ENUM

        # Following dictionaries transform enum value to string
        TASK_STATUS_ENUM_TO_STRING
        TASK_TYPE_ENUM_TO_STRING
        TASK_PRIORITY_ENUM_TO_STRING
        TASK_RELATION_ENUM_TO_STRING
        DATE_TYPE_ENUM_TO_STRING
        REPETITION_TYPE_ENUM_TO_STRING
        PLAN_COMPLETION_CRITERIA_ENUM_TO_STRING
        PLAN_STATE_ENUM_TO_STRING
        SUBTASK_DEPENDENCY_ENUM_TO_STRING
        USED_ENUM_TO_STRING
        ACTIVE_ENUM_TO_STRING

"""
from enum import Enum


class TaskStatus(Enum):
    DEFINED = 0
    CREATED = 1
    WORK = 2
    EXECUTED = 3
    OVERDUE = 4
    CLOSED = 5
    PAUSED = 6


class TaskType(Enum):
    SIMPLE = 0
    CONDITIONAL = 1
    PLAN = 2


class TaskPriority(Enum):
    MINOR = 0
    LOW = 1
    REGULAR = 2
    HIGH = 3
    CRITICAL = 4


class ActionOnExecutedTask(Enum):
    DELETE = 1
    CLOSE = 2
    MARK_AS_EXECUTED = 4


class ActionOnOverdueTask(Enum):
    MARK_AS_OVERDUE = 0
    DELETE = 1
    CLOSE = 2


class TaskRelation(Enum):
    BLOCKING = 0
    DEPENDS_ON = 1
    CONNECTED = 2


class GroupType(Enum):
    SYSTEM = 0
    CUSTOM = 1


class DateType(Enum):
    START = 0
    END = 1
    EVENT = 2


class RepetitionType(Enum):
    ANNUALLY = 0
    MONTHLY = 1
    WEEKLY = 2
    DAILY = 3


class PlanCompletionCriteria(Enum):
    NEVER = 0
    AFTER_NUMBER_OF_TIMES = 1
    AFTER_DATE = 2


class PlanState(Enum):
    ACTIVE = 0
    NOT_ACTIVE = 1


class RelationConfirmation(Enum):
    NOT_CONFIRMED = 0
    CONFIRMED = 1


class SubtaskDependency(Enum):
    NOT_DEPENDENT = 0
    DEPENDENT = 1


class Used(Enum):
    NO = 0
    YES = 1


class Active(Enum):
    NO = 0
    YES = 1


STATUS_DEFINED = "defined"
STATUS_CREATED = "created"
STATUS_WORK = "work"
STATUS_EXECUTED = "executed"
STATUS_OVERDUE = "overdue"
STATUS_CLOSED = "closed"
STATUS_PAUSED = "paused"

TASK_TYPE_SIMPLE = "simple"
TASK_TYPE_CONDITION = "condition"
TASK_TYPE_PLAN = "plan"

PRIORITY_MINOR = "minor"
PRIORITY_LOW = "low"
PRIORITY_REGULAR = "regular"
PRIORITY_HIGH = "high"
PRIORITY_CRITICAL = "critical"

SUBTASK_BLOCKING = "blocking"
SUBTASK_DEPENDS = "depends"
SUBTASK_CONNECTED = "connected"

DATE_TYPE_START = "start"
DATE_TYPE_END = "end"
DATE_TYPE_EVENT = "event"

NO = "no"
YES = "yes"

PLAN_REPETITION_DAILY = "daily"
PLAN_REPETITION_WEEKLY = "weekly"
PLAN_REPETITION_MONTHLY = "monthly"
PLAN_REPETITION_ANNUALLY = "annually"

PLAN_ACTIVE_STATE = "active"
PLAN_INACTIVE_STATE = "inactive"

PLAN_END_NEVER = "never"
PLAN_END_DATE = "date"
PLAN_END_NUMBER = "number"

RESTRICTED_UPDATE_TASK_STATUSES = [STATUS_DEFINED, STATUS_CREATED]

STRING_TO_TASK_STATUS_ENUM = {
     STATUS_DEFINED:   TaskStatus.DEFINED.value,
     STATUS_CREATED:   TaskStatus.CREATED.value,
     STATUS_WORK:   TaskStatus.WORK.value,
     STATUS_EXECUTED:   TaskStatus.EXECUTED.value,
     STATUS_OVERDUE:   TaskStatus.OVERDUE.value,
     STATUS_CLOSED:   TaskStatus.CLOSED.value,
     STATUS_PAUSED:   TaskStatus.PAUSED.value
}

STRING_TO_UPDATABLE_TASK_STATUSES = {key: value for key, value in STRING_TO_TASK_STATUS_ENUM.items()
                                     if key not in RESTRICTED_UPDATE_TASK_STATUSES}
STRING_TO_TASK_TYPE_ENUM = {
     TASK_TYPE_SIMPLE:   TaskType.SIMPLE.value,
     TASK_TYPE_CONDITION:   TaskType.CONDITIONAL.value,
     TASK_TYPE_PLAN:   TaskType.PLAN.value
}

STRING_TO_TASK_PRIORITY_ENUM = {
     PRIORITY_MINOR:   TaskPriority.MINOR.value,
     PRIORITY_LOW:   TaskPriority.LOW.value,
     PRIORITY_REGULAR:   TaskPriority.REGULAR.value,
     PRIORITY_HIGH:   TaskPriority.HIGH.value,
     PRIORITY_CRITICAL:   TaskPriority.CRITICAL.value
}

STRING_TO_TASK_RELATION_ENUM = {
     SUBTASK_BLOCKING:   TaskRelation.BLOCKING.value,
     SUBTASK_DEPENDS:   TaskRelation.DEPENDS_ON.value,
     SUBTASK_CONNECTED:   TaskRelation.CONNECTED.value
}

STRING_TO_DATE_TYPE_ENUM = {
     DATE_TYPE_START:   DateType.START.value,
     DATE_TYPE_END:   DateType.END.value,
     DATE_TYPE_EVENT:   DateType.EVENT.value
}

STRING_TO_REPETITION_TYPE_ENUM = {
     PLAN_REPETITION_ANNUALLY:   RepetitionType.ANNUALLY.value,
     PLAN_REPETITION_MONTHLY:   RepetitionType.MONTHLY.value,
     PLAN_REPETITION_WEEKLY:   RepetitionType.WEEKLY.value,
     PLAN_REPETITION_DAILY:   RepetitionType.DAILY.value
}

STRING_TO_PLAN_COMPLETION_CRITERIA_ENUM = {
     PLAN_END_NEVER:   PlanCompletionCriteria.NEVER.value,
     PLAN_END_DATE:   PlanCompletionCriteria.AFTER_DATE.value,
     PLAN_END_NUMBER:   PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value
}

STRING_TO_PLAN_STATE_ENUM = {
     PLAN_ACTIVE_STATE:   PlanState.ACTIVE.value,
     PLAN_INACTIVE_STATE:   PlanState.NOT_ACTIVE.value
}

STRING_TO_SUBTASK_DEPENDENCY_ENUM = {
     NO:   SubtaskDependency.NOT_DEPENDENT.value,
     YES:   SubtaskDependency.DEPENDENT.value
}

STRING_TO_USED_ENUM = {
     NO:   Used.NO.value,
     YES:   Used.YES.value
}

STRING_TO_ACTIVE_ENUM = {
     NO:   Active.NO.value,
     YES:   Active.YES.value
}


def reverse_dictionary(dictionary):
    return {v: k for k, v in dictionary.items()}


TASK_STATUS_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_STATUS_ENUM)

TASK_TYPE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_TYPE_ENUM)

TASK_PRIORITY_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_PRIORITY_ENUM)

TASK_RELATION_ENUM_TO_STRING = reverse_dictionary(STRING_TO_TASK_RELATION_ENUM)

DATE_TYPE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_DATE_TYPE_ENUM)

REPETITION_TYPE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_REPETITION_TYPE_ENUM)

PLAN_COMPLETION_CRITERIA_ENUM_TO_STRING = reverse_dictionary(STRING_TO_PLAN_COMPLETION_CRITERIA_ENUM)

PLAN_STATE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_PLAN_STATE_ENUM)

SUBTASK_DEPENDENCY_ENUM_TO_STRING = reverse_dictionary(STRING_TO_SUBTASK_DEPENDENCY_ENUM)

USED_ENUM_TO_STRING = reverse_dictionary(STRING_TO_USED_ENUM)

ACTIVE_ENUM_TO_STRING = reverse_dictionary(STRING_TO_ACTIVE_ENUM)
