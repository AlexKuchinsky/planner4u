"""
    Provides functions and classes for library
    configuration

    Classes:
        JSONConfigurator - read configurations from JSON file (format specified in class docs)

"""
import logging
import json
import os

from planner4ucore.services import log_functions


class JSONConfigurator:
    """
        Reads configurations from specified file

        Attributes:
            user_id(int): read from configuration file id of authorized user
            connection_string (str): read from configuration file connections string

        Default file name: "planner4Uconfig.json"
        Default file path - user's home id

        Example of configuration file:
        {
            # Required
            "user id": 1,
            # Required
            "connection string" : "mysql://user_name:password.@port/database_name",
            #Optional
             "log output": {
                "file":{
                    "level": "info",
                    # Path of log file
                    "file path": "mylog.log"
                },
                "terminal":{
                    "level": "warning"
                }
            }
        }

    """

    CONFIGURATION_FILE_EXAMPLE = """
    Example of valid configuration file:
            {
                # Required
                "user id": 1,
                # Required
                "connection string" : "mysql://user_name:password.@port/database_name",
                # Optional
                "log output": {
                    "file":{
                        "level": "info",
                        # Path of log file
                        "file path": "mylog.log"
                    },
                    "terminal":{
                        "level": "warning"
                    }
                }
            }
            
    """
    DEFAULT_FILE_NAME = "planner4Uconfig.json"

    def __init__(self, file_path=None):
        """
            Args:
                file_path (str) - file with configurations

            Raises:
                FileNotFoundError
                KeyError
                JSONDecodeError

        """
        if file_path is None:
            file_path = os.path.join(os.getenv("HOME"), self.DEFAULT_FILE_NAME)

        # Getting configuration dictionary
        with open(file_path, "r") as config:
            configuration = json.loads(config.read())

        # Checking required settings
        try:
            self.user_id = configuration["user id"]
            self.connection_string = configuration["connection string"]
        except KeyError:
            raise KeyError("Not all required configurations were specified (\"user id\", \"connection string\") ")

        # Setting up logging configurations
        if "log output" in configuration:
            self._configure_log_output(configuration["log output"])
        else:
            logging.basicConfig(level=logging.CRITICAL)

    @staticmethod
    def _configure_log_output(log_config):
        """
            Configures log output

            :raises
                KeyError
            :param log_config:
            :return:
        """
        levels = {
            log_functions.LOG_DEBUG: logging.DEBUG,
            log_functions.LOG_INFO: logging.INFO,
            log_functions.LOG_WARNING: logging.WARNING,
            log_functions.LOG_EXCEPTION: logging.ERROR,
            log_functions.LOG_CRITICAL: logging.CRITICAL
        }

        if "file" in log_config or "terminal" in log_config:
            # Set up base root logger (format and default log debug)
            log_format = logging.Formatter("%(levelname)s [%(name)s] [%(asctime)s] %(message)s")
            lib_logger = log_functions.get_planner4u_logger()
            lib_logger.setLevel(logging.DEBUG)

            # Override default terminal logger
            terminal_handler = logging.StreamHandler()
            terminal_handler.setFormatter(log_format)
            if "terminal" in log_config:
                try:
                    terminal_handler.setLevel(levels[log_config["terminal"]["level"]])
                except KeyError:
                    raise KeyError("Specify log level for log output - terminal")
            else:
                # Set log level for default terminal output logger
                terminal_handler.setLevel(logging.CRITICAL)
            # Add terminal handler to root logger
            lib_logger.addHandler(terminal_handler)

            # Configure file log output if specified
            if "file" in log_config:
                # Check settings
                try:
                    log_level = log_config["file"]["level"].lower()
                    file_handler = logging.FileHandler(log_config["file"]["file path"])
                    file_handler.setLevel(levels[log_level])
                    file_handler.setFormatter(log_format)
                    # Attach file handler to root handler
                    lib_logger.addHandler(file_handler)
                except KeyError:
                    raise KeyError("Pleace specify file path and log level in log output - file")
        else:
            raise KeyError(("You have defined \"log output\" in configfile, but haven\'t "
                            "specified output (file or terminal)"))
