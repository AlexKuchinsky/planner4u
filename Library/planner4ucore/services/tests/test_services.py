import unittest
from datetime import datetime
from dateutil.relativedelta import relativedelta

from planner4ucore.models import enums as me
from planner4ucore.storage.sqlalchemy_storage import SQLAlchemyStorage
from planner4ucore.models import models as mo
from planner4ucore.services.configurations import JSONConfigurator
from planner4ucore.services import services
from planner4ucore.services import exceptions as ex


class TaskTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        configuration = JSONConfigurator()

        data_storage = SQLAlchemyStorage(connection_string=configuration.connection_string)

        cls.serv = services.Service(data_storage)
        cls.ds = data_storage

    def setUp(self):
        self.user_creator = self.serv.add_user(mo.User("creator@"))
        self.user_rebel = self.serv.add_user(mo.User("rebel@"))
        self.task = mo.Task(task_type=me.TaskType.SIMPLE.value, user_creator=self.user_creator.user_id,
                            title="Test task", status=me.TaskStatus.CREATED.value)
        self.user_creator_id = self.user_creator.user_id
        self.user_rebel_id = self.user_rebel.user_id
        self.task = self.serv.add_task(self.task)

    def tearDown(self):
        self.serv.delete_user(self.user_creator_id)
        self.serv.delete_user(self.user_rebel_id)

    def test_update_task(self):
        user_id = self.user_creator_id
        update_args = {
            mo.TASK_ATTR_TITLE: "new updated title",
            mo.TASK_ATTR_DESCRIPTION: "new updated description",
            mo.TASK_ATTR_PRIORITY: me.TaskPriority.CRITICAL.value,
            mo.TASK_ATTR_DEPENDENT: me.SubtaskDependency.DEPENDENT.value,
            mo.TASK_ATTR_OVERDUE_PRIORITY: me.TaskPriority.LOW.value
        }
        self.serv.update_task(user_id, self.task.task_id, **update_args)
        updated_task = self.serv.get_task(user_id, self.task.task_id)

        self.assertEqual(updated_task.title, update_args[mo.TASK_ATTR_TITLE], "Task title update assert")
        self.assertEqual(updated_task.description, update_args[mo.TASK_ATTR_DESCRIPTION],
                         "Task description update assert")
        self.assertEqual(updated_task.priority, update_args[mo.TASK_ATTR_PRIORITY], "Task priority update assert")
        self.assertEqual(updated_task.last_subtask_dependency, update_args[mo.TASK_ATTR_DEPENDENT],
                         "Task last subtask execution assert")
        self.assertEqual(updated_task.priority_on_overdue, update_args[mo.TASK_ATTR_OVERDUE_PRIORITY],
                         "Task priority on overdue assert")

    def test_update_task_status(self):
        user_id = self.user_creator_id
        self.serv.update_task(user_id, self.task.task_id, status=me.TaskStatus.EXECUTED.value)

        updated_task = self.serv.get_task(user_id, self.task.task_id)
        self.assertEqual(updated_task.status, me.TaskStatus.EXECUTED.value, "Task status update assert")

    def test_executor_relation(self):
        user_id = self.user_creator_id

        relation = mo.UserExecutorRelation(self.user_rebel_id, self.task.task_id)
        self.serv.add_user_executor_relation(user_id, relation)

        # Checking that user was added
        executors_id = {user.user_id for user in self.serv.get_users_executors(user_id, self.task.task_id)}
        self.assertIn(self.user_rebel_id, executors_id, "User executor relation creation assert")

        # Checking duplication
        with self.assertRaises(ex.DuplicatedObjectError, msg="User executor relation duplication assert"):
            self.serv.add_user_executor_relation(user_id, relation)

        # Check that user was deleted
        self.serv.delete_user_executor_relation(user_id, self.task.task_id, self.user_rebel_id)
        executors_id = {user.user_id for user in self.serv.get_users_executors(user_id, self.task.task_id)}
        self.assertNotIn(self.user_rebel_id, executors_id, "User executor relation deletion assert")

    def test_task_authorization(self):
        with self.assertRaises(ex.AuthorizationError, msg="Get task authorization assert"):
            self.serv.get_task(self.user_rebel_id, self.task.task_id)

        with self.assertRaises(ex.AuthorizationError, msg="Update task authorization assert"):
            self.serv.update_task(self.user_rebel_id, self.task.task_id, priority=me.TaskPriority.LOW.value)

        with self.assertRaises(ex.AuthorizationError, msg="Delete task authorization assert"):
            self.serv.delete_task(self.user_rebel_id, self.task.task_id)

    def test_subtask_relation(self):
        user_id = self.user_creator_id

        subtask = mo.Task(user_id, "subtask")
        subtask = self.serv.add_task(subtask)

        # Add relation
        relation = mo.SubtaskRelation(self.task.task_id, subtask.task_id, me.TaskRelation.BLOCKING.value)
        self.serv.add_subtask_relation(user_id, relation)

        # Duplicated relation
        duplicated_relation = mo.SubtaskRelation(self.task.task_id, subtask.task_id, me.TaskRelation.BLOCKING.value)
        with self.assertRaises(ex.DuplicatedObjectError, msg="Subtask relation duplication assert"):
            self.serv.add_subtask_relation(user_id, duplicated_relation)

        # Check loop dependency
        loop_relation = mo.SubtaskRelation(subtask.task_id, self.task.task_id, me.TaskRelation.BLOCKING.value)
        with self.assertRaises(ex.RestrictionError, msg="Subtask loop dependency assert"):
            self.serv.add_subtask_relation(user_id, loop_relation)

        subtasks_id = [task.task_id for task in self.ds.get_sub_tasks(self.task.task_id)]
        self.assertIn(subtask.task_id, subtasks_id, "Subtask relation addition assert")

        parents_id = [task.task_id for task in self.ds.get_parent_tasks(subtask.task_id)]
        self.assertIn(self.task.task_id, parents_id, "Parent task relation addition assert")

        # Delete relation
        self.serv.delete_subtask_relation(user_id, self.task.task_id, subtask.task_id)
        subtasks_id = [task.task_id for task in self.ds.get_sub_tasks(self.task.task_id)]
        self.assertNotIn(subtask.task_id, subtasks_id, "Subtask relation deletion assert")

        parents_id = [task.task_id for task in self.ds.get_parent_tasks(subtask.task_id)]
        self.assertNotIn(self.task.task_id, parents_id, "Parent task relation deletion assert")

    def test_subtask_blocking_relation(self):
        user_id = self.user_creator_id

        subtask_1 = mo.Task(user_id, "subtask")
        subtask_1 = self.serv.add_task(subtask_1)

        subtask_2 = mo.Task(user_id, "subtask")
        subtask_2 = self.serv.add_task(subtask_2)

        # Add relation
        relation_1 = mo.SubtaskRelation(self.task.task_id, subtask_1.task_id, me.TaskRelation.BLOCKING.value)
        self.serv.add_subtask_relation(user_id, relation_1)

        relation_2 = mo.SubtaskRelation(self.task.task_id, subtask_2.task_id, me.TaskRelation.DEPENDS_ON.value)
        self.serv.add_subtask_relation(user_id, relation_2)

        # Change status of parent task on executed
        with self.assertRaises(ex.DependencyError, msg="Blocking subtask relation assert"):
            self.serv.update_task(user_id, self.task.task_id, status=me.TaskStatus.EXECUTED.value)

        subtask_2 = self.serv.get_task(user_id, subtask_2.task_id)
        self.assertEqual(subtask_2.status, me.TaskStatus.CREATED.value,
                         "Ignoring task failed update assert")

        # Check complex dependency tree
        self.serv.update_subtask_relation(user_id, relation_1.relation_id, me.TaskRelation.CONNECTED.value)

        subtask_3 = mo.Task(user_id, "subtask")
        subtask_3 = self.serv.add_task(subtask_3)

        subtask_4 = mo.Task(user_id, "subtask")
        subtask_4 = self.serv.add_task(subtask_4)

        # Add relation
        relation_3 = mo.SubtaskRelation(subtask_2.task_id, subtask_3.task_id, me.TaskRelation.DEPENDS_ON.value)
        self.serv.add_subtask_relation(user_id, relation_3)

        relation_4 = mo.SubtaskRelation(subtask_1.task_id, subtask_4.task_id, me.TaskRelation.DEPENDS_ON.value)
        self.serv.add_subtask_relation(user_id, relation_4)

        # Now update task root task status
        self.serv.update_task(user_id, self.task.task_id, status=me.TaskStatus.EXECUTED.value)

        subtask_1 = self.serv.get_task(user_id, subtask_1.task_id)
        subtask_2 = self.serv.get_task(user_id, subtask_2.task_id)
        subtask_3 = self.serv.get_task(user_id, subtask_3.task_id)
        subtask_4 = self.serv.get_task(user_id, subtask_4.task_id)

        self.assertEqual(subtask_1.status, me.TaskStatus.CREATED.value,
                         "Subtask status (connected) wasn's changed assert")
        self.assertEqual(subtask_2.status, me.TaskStatus.EXECUTED.value,
                         "Subtask status (dependent) was changed on executed assert")
        self.assertEqual(subtask_3.status, me.TaskStatus.EXECUTED.value,
                         "Subtask status of subtask (dependent-dependent) was changed on executed assert")
        self.assertEqual(subtask_4.status, me.TaskStatus.CREATED.value,
                         "Subtask status of subtask (connected-dependent) wasn't changed assert")

    def test_subtask_depends_relation(self):
        user_id = self.user_creator_id

        subtask = mo.Task(user_id, "subtask")
        subtask = self.serv.add_task(subtask)

        # Add relation
        relation = mo.SubtaskRelation(self.task.task_id, subtask.task_id, me.TaskRelation.DEPENDS_ON.value)
        self.serv.add_subtask_relation(user_id, relation)

        # Change status of parent task on executed
        self.serv.update_task(user_id, self.task.task_id, status=me.TaskStatus.EXECUTED.value)

        subtask = self.serv.get_task(user_id, subtask.task_id)
        self.assertEqual(subtask.status, me.TaskStatus.EXECUTED.value,
                         "Subtask relation (dependent) assert")

    def test_parent_task_subtask_dependency(self):
        user_id = self.user_creator_id

        # Setting last subtask dependency
        self.task = self.serv.update_task(user_id, self.task.task_id, status=me.TaskStatus.WORK.value,
                                          last_subtask_dependency=me.SubtaskDependency.DEPENDENT.value)

        first_subtask = mo.Task(user_id, "subtask1")
        first_subtask = self.serv.add_task(first_subtask)

        second_subtask = mo.Task(user_id, "subtask2")
        second_subtask = self.serv.add_task(second_subtask)

        # Add relation for first subtask
        first_relation = mo.SubtaskRelation(self.task.task_id, first_subtask.task_id, me.TaskRelation.CONNECTED.value)
        self.serv.add_subtask_relation(user_id, first_relation)

        # Add relation for second subtask
        second_relation = mo.SubtaskRelation(self.task.task_id, second_subtask.task_id, me.TaskRelation.CONNECTED.value)
        self.serv.add_subtask_relation(user_id, second_relation)

        # Change status of first subtask
        self.serv.update_task(user_id, first_subtask.task_id, status=me.TaskStatus.EXECUTED.value)

        # Check that parent task wasn't executed
        self.task = self.serv.get_task(user_id, self.task.task_id)
        self.assertNotEqual(self.task.status, me.TaskStatus.EXECUTED.value,
                            "Not dependent on last subtask execution parent task status assert")

        # Change status of second subtask
        self.serv.update_task(user_id, second_subtask.task_id, status=me.TaskStatus.EXECUTED.value)
        # Check that parent was executed
        self.task = self.serv.get_task(user_id, self.task.task_id)
        self.assertEqual(self.task.status, me.TaskStatus.EXECUTED.value,
                         "Dependent on last subtak execution parent task status assert")

    def test_overdue_task(self):
        user_id = self.user_creator_id
        # Set priority on overdue
        self.serv.update_task(user_id, self.task.task_id,
                              priority_on_overdue=me.TaskPriority.CRITICAL.value)
        overdue_date = datetime.now().date() - relativedelta(days=2)

        event_date = mo.TaskDate(self.task.task_id, me.DateType.EVENT.value, overdue_date)

        self.serv.add_date(user_id, event_date)
        self.serv.update_overdue_tasks()

        self.task = self.serv.get_task(user_id, self.task.task_id)
        self.assertEqual(self.task.status, me.TaskStatus.OVERDUE.value,
                         "Overdue date task status assert")
        self.assertEqual(self.task.priority, me.TaskPriority.CRITICAL.value,
                         "Priority on overdue for task assert")

    def test_delete_task(self):
        user_id = self.user_creator_id
        task_id = self.task.task_id
        self.serv.delete_task(user_id, task_id)
        with self.assertRaises(ex.ObjectIsNotFoundError, msg="Delete task (object not found) assert"):
            self.serv.get_task(user_id, task_id)

    def test_get_filtered_tasks(self):
        task_1 = self.serv.add_task(mo.Task(self.user_creator_id, "Test_task_1"))
        task_2 = self.serv.add_task(mo.Task(self.user_creator_id, "Test_task_2"))

        one_task_filter_result = self.serv.get_filtered_tasks(task_id=task_1.task_id)[0]
        self.assertEqual(one_task_filter_result.task_id, task_1.task_id, "Task filtered by id assert")

        tasks_result = self.serv.get_filtered_tasks(user_creator_id=self.user_creator.name)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertIn(task_1.task_id, tasks_id_result, "Task filtered by user creator")
        self.assertIn(task_2.task_id, tasks_id_result, "Task filtered by user creator")
        self.assertIn(self.task.task_id, tasks_id_result, "Task filtered by user creator")

        self.serv.update_task(self.user_creator_id, task_1.task_id, status=me.TaskStatus.CLOSED.value)
        self.serv.update_task(self.user_creator_id, task_2.task_id, status=me.TaskStatus.WORK.value)

        tasks_result = self.serv.get_filtered_tasks(status=me.TaskStatus.CLOSED.value)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertIn(task_1.task_id, tasks_id_result, "Filter by status assert in")
        self.assertNotIn(task_2.task_id, tasks_id_result, "Filter by statys assert not in")

        current_date = datetime.now().date()
        two_days_delta = relativedelta(days=2)
        four_days_delta = relativedelta(days=4)

        event_date_1 = mo.TaskDate(task_1.task_id, me.DateType.EVENT.value, current_date+two_days_delta)
        event_date_1 = self.serv.add_date(self.user_creator_id, event_date_1)

        event_date_2 = mo.TaskDate(task_2.task_id, me.DateType.EVENT.value, current_date-four_days_delta)
        event_date_2 = self.serv.add_date(self.user_creator_id, event_date_2)

        tasks_result = self.serv.get_filtered_tasks(from_date=current_date)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertIn(task_1.task_id, tasks_id_result, "Filter by from date assert in")
        self.assertNotIn(task_2.task_id, tasks_id_result, "Filter by from date assert not in")

        tasks_result = self.serv.get_filtered_tasks(from_date=current_date-four_days_delta)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertIn(task_1.task_id, tasks_id_result, "Filter by from date assert in")
        self.assertIn(task_2.task_id, tasks_id_result, "Filter by from date assert in")

        tasks_result = self.serv.get_filtered_tasks(to_date=current_date-two_days_delta)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertNotIn(task_1.task_id, tasks_id_result, "Filter by from date assert not in")
        self.assertIn(task_2.task_id, tasks_id_result, "Filter by from date assert in")

        tasks_result = self.serv.get_filtered_tasks(to_date=current_date-two_days_delta)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertNotIn(task_1.task_id, tasks_id_result, "Filter by from date assert not in")
        self.assertIn(task_2.task_id, tasks_id_result, "Filter by from date assert in")

        tasks_result = self.serv.get_filtered_tasks(to_date=current_date+four_days_delta,
                                                    from_date=current_date-two_days_delta)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertIn(task_1.task_id, tasks_id_result, "Filter by from date assert in")
        self.assertNotIn(task_2.task_id, tasks_id_result, "Filter by from date assert not in")

        tasks_result = self.serv.get_filtered_tasks(to_date=current_date+four_days_delta,
                                                    from_date=current_date-four_days_delta)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertIn(task_1.task_id, tasks_id_result, "Filter by from date assert in")
        self.assertIn(task_2.task_id, tasks_id_result, "Filter by from date assert in")

        tasks_result = self.serv.get_filtered_tasks(to_date=current_date,
                                                    from_date=current_date)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertNotIn(task_1.task_id, tasks_id_result, "Filter by from date assert not in")
        self.assertNotIn(task_2.task_id, tasks_id_result, "Filter by from date assert not in")

        tasks_result = self.serv.get_filtered_tasks(user_executor_id=self.user_rebel.name)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertNotIn(task_1.task_id, tasks_id_result, "Filter by from date assert not in")
        self.assertNotIn(task_2.task_id, tasks_id_result, "Filter by from date assert not in")

        self.serv.add_user_executor_relation(self.user_creator_id,
                                             mo.UserExecutorRelation(self.user_rebel_id, task_1.task_id))

        tasks_result = self.serv.get_filtered_tasks(user_executor_id=self.user_rebel.name)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertIn(task_1.task_id, tasks_id_result, "Filter by from date assert  in")
        self.assertNotIn(task_2.task_id, tasks_id_result, "Filter by from date assert not in")

        self.serv.add_user_executor_relation(self.user_creator_id,
                                             mo.UserExecutorRelation(self.user_rebel_id, task_2.task_id))
        tasks_result = self.serv.get_filtered_tasks(user_executor_id=self.user_rebel.name)
        tasks_id_result = {t.task_id for t in tasks_result}
        self.assertIn(task_1.task_id, tasks_id_result, "Filter by from date assert  in")
        self.assertIn(task_2.task_id, tasks_id_result, "Filter by from date assert  in")


class PlanTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        configuration = JSONConfigurator()

        data_storage = SQLAlchemyStorage(connection_string=configuration.connection_string)

        cls.serv = services.Service(data_storage)
        cls.ds = data_storage

    def setUp(self):
        self.user = self.serv.add_user(mo.User("creator@"))
        self.user_id = self.user.user_id
        self.template_task = mo.Task(self.user_id, "Plan task", task_type=me.TaskType.PLAN.value)

    def tearDown(self):
        self.serv.delete_user(self.user_id)

    def test_plan_creation(self):
        interval = 6
        plan = mo.Plan(create_task_date=datetime.now().date(), repeat_type=me.RepetitionType.ANNUALLY.value,
                       plan_interval=interval, end_type=me.PlanCompletionCriteria.NEVER.value)

        _, plan = self.serv.add_plan(self.template_task, plan)

        user_tasks_count_before = len(self.serv.get_user_tasks(self.user_id))
        plan_created_tasks_count_before = plan.repetitions_count
        expecting_date = datetime.now().date() + relativedelta(years=interval)

        self.serv.update_ripe_plans()
        plan = self.serv.get_plan(self.user_id, plan.plan_id)

        user_tasks_count_after = len(self.serv.get_user_tasks(self.user_id))
        plan_created_tasks_count_after = plan.repetitions_count
        next_creation_date = plan.create_task_date

        self.assertEqual(user_tasks_count_after - user_tasks_count_before, 1,
                         "One plan task was created for user assert")
        self.assertEqual(plan_created_tasks_count_after - plan_created_tasks_count_before, 1,
                         "Plan count of created tasks was incremented assert")
        self.assertEqual(expecting_date, next_creation_date,
                         "Next date of template task creation assert")

    def test_plan_completion_date(self):
        past_date = datetime.now().date() - relativedelta(days=2)
        plan = mo.Plan(create_task_date=datetime.now().date(), repeat_type=me.RepetitionType.WEEKLY.value,
                       plan_interval=10, end_type=me.PlanCompletionCriteria.AFTER_DATE.value, end_date=past_date)
        _, plan = self.serv.add_plan(self.template_task, plan)

        user_tasks_count_before = len(self.serv.get_user_tasks(self.user_id))
        plan_created_tasks_count_before = plan.repetitions_count

        self.serv.update_ripe_plans()
        plan = self.serv.get_plan(self.user_id, plan.plan_id)

        user_tasks_count_after = len(self.serv.get_user_tasks(self.user_id))
        plan_created_tasks_count_after = plan.repetitions_count

        self.assertEqual(user_tasks_count_after, user_tasks_count_before,
                         "Plan completion date assert")
        self.assertEqual(plan_created_tasks_count_after, plan_created_tasks_count_before,
                         "Plan completion date assert")
        self.assertEqual(plan.active, me.Active.NO.value,
                         "Plan deactivation after completion date assert")

    def test_plan_completion_number_of_times(self):
        plan = mo.Plan(create_task_date=datetime.now().date(), repeat_type=me.RepetitionType.WEEKLY.value,
                       plan_interval=10, end_type=me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value,
                       repetitions_number=1)
        _, plan = self.serv.add_plan(self.template_task, plan)

        user_tasks_count_before = len(self.serv.get_user_tasks(self.user_id))
        plan_created_tasks_count_before = plan.repetitions_count

        self.serv.update_ripe_plans()
        plan = self.serv.get_plan(self.user_id, plan.plan_id)

        user_tasks_count_after = len(self.serv.get_user_tasks(self.user_id))
        plan_created_tasks_count_after = plan.repetitions_count

        self.assertEqual(user_tasks_count_after - user_tasks_count_before, 1,
                         "One plan task was created for user assert")
        self.assertEqual(plan_created_tasks_count_after - plan_created_tasks_count_before, 1,
                         "Plan count of created tasks was incremented assert")
        self.assertEqual(plan.active, me.Active.NO.value,
                         "Plan deactivation after number of times date assert")

    def test_plan_restrictions(self):
        plan = mo.Plan(create_task_date=datetime.now().date(), repeat_type=me.RepetitionType.WEEKLY.value,
                       plan_interval=10, end_type=me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value,
                       repetitions_number=1)
        self.template_task, plan = self.serv.add_plan(self.template_task, plan)

        with self.assertRaises(ex.RestrictionError, msg="Template task update status restriction assert"):
            self.serv.update_task(self.user_id, self.template_task.task_id, status=me.TaskStatus.EXECUTED.value)

        with self.assertRaises(ex.RestrictionError, msg="Template task add date restriction assert"):
            date = mo.TaskDate(self.template_task.task_id, me.DateType.EVENT.value, datetime.now().date())
            self.serv.add_date(self.user_id, date)

        with self.assertRaises(ex.RestrictionError, msg="Template task add reminder restriction assert"):
            reminder = mo.Reminder(self.template_task.task_id, datetime.now().date(),
                                   datetime.now().time(), 10)
            self.serv.add_reminder(self.user_id, reminder)

    def test_plan_authorization(self):
        plan = mo.Plan(create_task_date=datetime.now().date(), repeat_type=me.RepetitionType.WEEKLY.value,
                       plan_interval=10, end_type=me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value,
                       repetitions_number=1)
        self.template_task, plan = self.serv.add_plan(self.template_task, plan)

        user = mo.User("test@")
        user = self.serv.add_user(user)
        with self.assertRaises(ex.AuthorizationError, msg="Plan authorization assert"):
            self.serv.get_plan(user.user_id, plan.plan_id)

        self.serv.delete_user(user.user_id)

    def test_plan_update(self):
        plan = mo.Plan(create_task_date=datetime.now().date(), repeat_type=me.RepetitionType.WEEKLY.value,
                       plan_interval=10, end_type=me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value,
                       repetitions_number=1)
        self.template_task, plan = self.serv.add_plan(self.template_task, plan)

        update_args = {
            mo.PLAN_ATTR_TYPE: me.RepetitionType.ANNUALLY.value,
            mo.PLAN_ATTR_INTERVAL: 80,
            mo.PLAN_ATTR_DATE: datetime.now().date() + relativedelta(weeks=9, days=2),
        }

        self.serv.update_plan(self.user_id, plan.plan_id, repeat_type=me.RepetitionType.ANNUALLY.value,
                              plan_interval=80, create_task_date=datetime.now().date() + relativedelta(weeks=9, days=2))
        plan = self.serv.get_plan(self.user_id, plan.plan_id)

        self.assertEqual(plan.repeat_type, update_args[mo.PLAN_ATTR_TYPE], "Update plan repeat type assert")
        self.assertEqual(plan.plan_interval, update_args[mo.PLAN_ATTR_INTERVAL], "Update plan interval assert")
        self.assertEqual(plan.create_task_date, update_args[mo.PLAN_ATTR_DATE], "Update creation task date assert")

    def test_delete_plan(self):
        plan = mo.Plan(create_task_date=datetime.now().date(), repeat_type=me.RepetitionType.WEEKLY.value,
                       plan_interval=10, end_type=me.PlanCompletionCriteria.AFTER_NUMBER_OF_TIMES.value,
                       repetitions_number=1)
        self.template_task, plan = self.serv.add_plan(self.template_task, plan)

        self.serv.delete_task(self.user_id, self.template_task.task_id)

        with self.assertRaises(ex.ObjectIsNotFoundError, msg="Deletion template task assert"):
            self.serv.get_task(self.user_id, self.template_task.task_id)

        with self.assertRaises(ex.ObjectIsNotFoundError, msg="Deletion plan object assert"):
            self.serv.get_plan(self.user_id, plan.plan_id)


class ConditionTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        configuration = JSONConfigurator()

        data_storage = SQLAlchemyStorage(connection_string=configuration.connection_string)

        cls.serv = services.Service(data_storage)
        cls.ds = data_storage

    def setUp(self):
        self.user = self.serv.add_user(mo.User("creator@"))
        self.user_id = self.user.user_id
        self.condition_task = mo.Task(task_type=me.TaskType.SIMPLE.value, user_creator=self.user_id,
                                      title="Trigger task", status=me.TaskStatus.CREATED.value)
        self.defined_task = mo.Task(task_type=me.TaskType.CONDITIONAL.value, user_creator=self.user_id,
                                    title="Condition task", status=me.TaskStatus.DEFINED.value)
        self.condition_task = self.serv.add_task(self.condition_task)
        self.defined_task = self.serv.add_task(self.defined_task)

        self.relation = mo.ConditionTaskRelation(defined_task_id=self.defined_task.task_id,
                                                 condition_task_id=self.condition_task.task_id,
                                                 condition_task_status=me.TaskStatus.OVERDUE.value)

    def tearDown(self):
        self.serv.delete_user(self.user_id)

    def test_condition_creation(self):
        self.relation.condition_task_status = me.TaskStatus.CLOSED.value
        self.relation = self.serv.add_condition_relation(self.user_id, self.relation)

        self.serv.update_task(self.user_id, self.condition_task.task_id, status=me.TaskStatus.CLOSED.value)

        self.defined_task = self.serv.get_task(self.user_id, self.defined_task.task_id)
        self.assertEqual(self.defined_task.task_type, me.TaskType.SIMPLE.value,
                         "Creation of condition task assert(type)")
        self.assertEqual(self.defined_task.status, me.TaskStatus.CREATED.value,
                         "Creation of condition task assert(status)")

        with self.assertRaises(ex.ObjectIsNotFoundError,
                               msg="Condition object deletion after defined task creation assert"):
            self.serv.get_condition_task_relation(self.user_id, self.relation.relation_id)

    def test_condition_on_overdue_creation(self):
        self.relation.condition_task_status = me.TaskStatus.OVERDUE.value
        self.relation = self.serv.add_condition_relation(self.user_id, self.relation)

        past_date = datetime.now().date() - relativedelta(days=2)
        date = mo.TaskDate(task_id=self.condition_task.task_id, date_type=me.DateType.END.value,
                           date=past_date)
        self.serv.add_date(self.user_id, date)
        self.serv.update_overdue_tasks()

        self.defined_task = self.serv.get_task(self.user_id, self.defined_task.task_id)
        self.assertEqual(self.defined_task.task_type, me.TaskType.SIMPLE.value,
                         "Creation of condition task assert(type)")
        self.assertEqual(self.defined_task.status, me.TaskStatus.CREATED.value,
                         "Creation of condition task assert(status)")

        with self.assertRaises(ex.ObjectIsNotFoundError,
                               msg="Condition object deletion after defined task creation assert"):
            self.serv.get_condition_task_relation(self.user_id, self.relation.relation_id)

    def test_update_condition_relation(self):
        self.relation.condition_task_status = me.TaskStatus.OVERDUE.value
        self.relation = self.serv.add_condition_relation(self.user_id, self.relation)
        self.serv.update_condition_relation(self.user_id, self.relation.relation_id,
                                            condition_task_status=me.TaskStatus.EXECUTED.value)
        self.relation = self.serv.get_condition_task_relation(self.user_id, self.relation.relation_id)
        self.assertEqual(self.relation.condition_task_status, me.TaskStatus.EXECUTED.value,
                         "Update condition relation (condition task status) assert")

    def test_condition_restrictions(self):
        with self.assertRaises(ex.RestrictionError, msg="Condition task update status restriction assert"):
            self.serv.update_task(self.user_id, self.defined_task.task_id, status=me.TaskStatus.EXECUTED.value)

        with self.assertRaises(ex.RestrictionError, msg="Add date to condition task restriction assert"):
            date = mo.TaskDate(self.defined_task.task_id, me.DateType.EVENT.value, datetime.now().date())
            self.serv.add_date(self.user_id, date)

        with self.assertRaises(ex.RestrictionError, msg="Add reminder to condition task restriction assert"):
            reminder = mo.Reminder(self.defined_task.task_id, datetime.now().date(),
                                   datetime.now().time(), 10)
            self.serv.add_reminder(self.user_id, reminder)

    def test_condition_authorization(self):
        self.relation = self.serv.add_condition_relation(self.user_id, self.relation)

        rebel = mo.User("rebel@")
        rebel = self.serv.add_user(rebel)

        with self.assertRaises(ex.AuthorizationError, msg="Get condition relation authorizaton assert"):
            self.serv.get_condition_task_relation(rebel.user_id, self.relation.relation_id)

        with self.assertRaises(ex.AuthorizationError, msg="Get condtiiton relation authorization assert"):
            self.serv.get_condition_task_relation_by_defined(rebel.user_id, self.defined_task.task_id)

        with self.assertRaises(ex.AuthorizationError, msg="Delete condtition relation authorization assert"):
            self.serv.delete_condition_relation(rebel.user_id, self.relation.relation_id)

        # Add condition on not your task

        rebel_task = mo.Task(rebel.user_id, "test_task")
        rebel_task = self.serv.add_task(rebel_task)

        fake_relation = mo.ConditionTaskRelation(self.defined_task.task_id, rebel_task.task_id,
                                                 me.TaskStatus.CREATED.value)

        with self.assertRaises(ex.AuthorizationError,
                               msg="Condition dependency on another task authorization assert"):
            self.serv.add_condition_relation(self.user_id, fake_relation)

        self.serv.delete_user(rebel.user_id)

    def test_delete_condition(self):
        self.relation = self.serv.add_condition_relation(self.user_id, self.relation)
        self.serv.delete_task(self.user_id, self.defined_task.task_id)
        with self.assertRaises(ex.ObjectIsNotFoundError, msg="Condition taks deletion assert"):
            self.serv.get_condition_task_relation(self.user_id, self.relation.relation_id)


class TaskDateTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        configuration = JSONConfigurator()

        data_storage = SQLAlchemyStorage(connection_string=configuration.connection_string)

        cls.serv = services.Service(data_storage)
        cls.ds = data_storage

    def setUp(self):
        self.user = self.serv.add_user(mo.User("creator@"))
        self.user_id = self.user.user_id
        self.task = mo.Task(task_type=me.TaskType.SIMPLE.value, user_creator=self.user_id,
                            title="test task", status=me.TaskStatus.CREATED.value)
        self.task = self.serv.add_task(self.task)
        self.date = mo.TaskDate(task_id=self.task.task_id, date_type=me.DateType.EVENT.value,
                                date=datetime.now().date())

    def tearDown(self):
        self.serv.delete_user(self.user_id)

    def test_date_update(self):
        self.date = self.serv.add_date(self.user_id, self.date)

        new_date = datetime.now().date() + relativedelta(days=8, weeks=1)

        self.serv.update_date(self.user_id, self.date.date_id, date=new_date)

        self.date = self.serv.get_date(self.user_id, self.date.date_id)

        self.assertEqual(self.date.date, new_date, "Update task date assert")

    def test_date_restrictions(self):
        self.date.date_type = me.DateType.EVENT.value
        self.date = self.serv.add_date(self.user_id, self.date)

        test_date = mo.TaskDate(task_id=self.task.task_id, date_type=me.DateType.EVENT.value,
                                date=datetime.now().date())

        # Duplications checking of event
        with self.assertRaises(ex.RestrictionError, msg="Add second event date to task assert"):
            self.serv.add_date(self.user_id, test_date)

        test_date.date_type = me.DateType.END.value
        with self.assertRaises(ex.RestrictionError, msg="Add end date to task with event date assert"):
            self.serv.add_date(self.user_id, test_date)

        test_date.date_type = me.DateType.START.value
        with self.assertRaises(ex.RestrictionError, msg="Add start date to task with event date assert"):
            self.serv.add_date(self.user_id, test_date)

        # Of start date
        self.date = self.serv.update_date(self.user_id, self.date.date_id, date=datetime.now().date(),
                                          date_type=me.DateType.START.value)
        test_date.date_type = me.DateType.START.value
        with self.assertRaises(ex.RestrictionError, msg="Add second start date to task assert"):
            self.serv.add_date(self.user_id, test_date)

        test_date.date_type = me.DateType.EVENT.value
        with self.assertRaises(ex.RestrictionError, msg="Add event date to task with stat date assert"):
            self.serv.add_date(self.user_id, test_date)

        # Periods checking
        time_delta = relativedelta(days=5, weeks=1)
        self.date = self.serv.update_date(self.user_id, self.date.date_id, date_type=me.DateType.START.value,
                                          date=datetime.now().date())
        test_date.date_type = me.DateType.END.value
        test_date.date = self.date.date - time_delta

        with self.assertRaises(ex.RestrictionError, msg="Add end date before start restriction assert"):
            self.serv.add_date(self.user_id, test_date)

        self.date = self.serv.update_date(self.user_id, self.date.date_id, date_type=me.DateType.END.value,
                                          date=datetime.now().date())
        test_date.date_type = me.DateType.START.value
        test_date.date = self.date.date + time_delta

        with self.assertRaises(ex.RestrictionError, msg="Add start date after end date restriction assert"):
            self.serv.add_date(self.user_id, test_date)

    def test_date_authorization(self):
        self.date.date_type = me.DateType.EVENT.value
        self.date = self.serv.add_date(self.user_id, self.date)

        rebel = mo.User("rebel@")
        rebel = self.serv.add_user(rebel)

        with self.assertRaises(ex.AuthorizationError, msg="Get message authorization assert"):
            self.serv.get_date(rebel.user_id, self.date.date_id)

        with self.assertRaises(ex.AuthorizationError, msg="Update date authorization assert"):
            self.serv.update_date(rebel.user_id, self.date.date_id, date=datetime.now().date())

        with self.assertRaises(ex.AuthorizationError, msg="Delete date authorization assert"):
            self.serv.delete_date(rebel.user_id, self.date.date_id)

        self.serv.delete_user(rebel.user_id)

    def test_date_delete(self):
        self.date.date_type = me.DateType.EVENT.value
        self.date = self.serv.add_date(self.user_id, self.date)

        self.serv.delete_date(self.user_id, self.date.date_id)

        with self.assertRaises(ex.ObjectIsNotFoundError, msg="Task date deletion authorization assert"):
            self.serv.get_date(self.user_id, self.date.date_id)


class GroupTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        configuration = JSONConfigurator()

        data_storage = SQLAlchemyStorage(connection_string=configuration.connection_string)

        cls.serv = services.Service(data_storage)
        cls.ds = data_storage

    def setUp(self):
        self.user = self.serv.add_user(mo.User("creator@"))
        self.user_id = self.user.user_id

        self.group = mo.Group(self.user_id, "test group")
        self.group = self.serv.add_group(self.group)

        self.task_1 = mo.Task(task_type=me.TaskType.SIMPLE.value, user_creator=self.user_id,
                              title="test task", status=me.TaskStatus.CREATED.value)
        self.task_1 = self.serv.add_task(self.task_1)

        self.task_2 = mo.Task(task_type=me.TaskType.SIMPLE.value, user_creator=self.user_id,
                              title="test task", status=me.TaskStatus.CREATED.value)
        self.task_2 = self.serv.add_task(self.task_2)

    def tearDown(self):
        self.serv.delete_user(self.user_id)

    def test_group_task_relation(self):
        # Add relations
        relation_1 = mo.TaskGroupRelation(self.task_1.task_id, self.group.group_id)
        relation_1 = self.serv.add_task_group_relation(self.user_id, relation_1)

        relation_2 = mo.TaskGroupRelation(self.task_2.task_id, self.group.group_id)
        self.serv.add_task_group_relation(self.user_id, relation_2)

        group_tasks_id = [task.task_id for task in self.ds.get_group_tasks(self.group.group_id)]

        # Check that was added
        self.assertIn(self.task_1.task_id, group_tasks_id, "Addition task to group assert")
        self.assertIn(self.task_2.task_id, group_tasks_id, "Addition task to group assert")

        # Duplication checking
        with self.assertRaises(ex.DuplicatedObjectError, msg="Task group duplication assert"):
            self.serv.add_task_group_relation(self.user_id, relation_1)

        # Delete relations
        self.serv.delete_task_group_relation(self.user_id, relation_1.task_id, relation_1.group_id)
        group_tasks_id = [task.task_id for task in self.ds.get_group_tasks(self.group.group_id)]

        self.assertNotIn(self.task_1.task_id, group_tasks_id, "Deletion of task from group assert")
        self.assertIn(self.task_2.task_id, group_tasks_id)

        # Update group
        new_title = "new title of gorup "
        self.serv.update_group(self.user_id, self.group.group_id, title=new_title)
        self.group = self.serv.get_group(self.user_id, self.group.group_id)
        self.assertEqual(self.group.title, new_title, "Update group title assert")

    def test_group_authorizations(self):
        rebel = mo.User("rebel@")
        rebel = self.serv.add_user(rebel)

        rebel_task = mo.Task(rebel.user_id, "rebel task")
        rebel_task = self.serv.add_task(rebel_task)

        with self.assertRaises(ex.AuthorizationError, msg="Get group authorization assert"):
            self.serv.get_group(rebel.user_id, self.group.group_id)

        with self.assertRaises(ex.AuthorizationError, msg="Update group authorization assert"):
            self.serv.update_group(rebel.user_id, self.group.group_id, title="Title")

        with self.assertRaises(ex.AuthorizationError, msg="Delete group authorization assert"):
            self.serv.delete_group(rebel.user_id, self.group.group_id)

        relation = mo.TaskGroupRelation(rebel_task.task_id, self.group.group_id)
        with self.assertRaises(ex.AuthorizationError, msg="Addition task by another user restriction assert"):
            self.serv.add_task_group_relation(rebel.user_id, relation)

        with self.assertRaises(ex.AuthorizationError,
                               msg="Addition to group task of another user restrinction assert"):
            self.serv.add_task_group_relation(self.user_id, relation)

        self.serv.delete_user(rebel.user_id)

    def test_group_delete(self):
        self.serv.delete_group(self.user_id, self.group.group_id)

        with self.assertRaises(ex.ObjectIsNotFoundError, msg="Group deletion assert"):
            self.serv.get_group(self.user_id, self.group.group_id)


class ReminderTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        configuration = JSONConfigurator()

        data_storage = SQLAlchemyStorage(connection_string=configuration.connection_string)

        cls.serv = services.Service(data_storage)
        cls.ds = data_storage

    def setUp(self):
        self.user = self.serv.add_user(mo.User("creator@"))
        self.user_id = self.user.user_id

        self.task = mo.Task(task_type=me.TaskType.SIMPLE.value, user_creator=self.user_id,
                            title="test task", status=me.TaskStatus.CREATED.value)
        self.task = self.serv.add_task(self.task)

        self.reminder = mo.Reminder(self.task.task_id, datetime.now().date(), datetime.now().time(), 10)
        self.reminder = self.serv.add_reminder(self.user_id, self.reminder)
        self.reminder_id = self.reminder.reminder_id

    def tearDown(self):
        self.serv.delete_user(self.user_id)

    def test_reminder_creation(self):
        current_date = datetime.now().date()
        date_delta = relativedelta(days=2)
        current_time = datetime.now().time()
        days_interval = 10

        reminder = mo.Reminder(self.task.task_id, current_date + date_delta, current_time, days_interval)
        reminder = self.serv.add_reminder(self.user_id, reminder)

        # Try to update not ripe reminder
        self.serv.update_ripe_reminders()
        reminder = self.serv.get_reminder(self.user_id, reminder.reminder_id)
        self.assertEqual(reminder.raise_date, current_date + date_delta,
                         "Not updated raw reminder date assert")

        # Update reminder setting past date
        self.serv.update_reminder(self.user_id, reminder.reminder_id, raise_date=current_date-date_delta)
        self.serv.update_ripe_reminders()
        reminder = self.serv.get_reminder(self.user_id, reminder.reminder_id)
        self.assertEqual(reminder.raise_date, current_date-date_delta + relativedelta(days=days_interval),
                         "updated ripe reminder date assert")

    def test_reminder_authorization(self):
        rebel = mo.User("rebel@")
        rebel = self.serv.add_user(rebel)

        with self.assertRaises(ex.AuthorizationError, msg="Add reminder authorization assert"):
            self.serv.add_reminder(rebel.user_id, self.reminder)

        with self.assertRaises(ex.AuthorizationError, msg="Get reminder authorization assert"):
            self.serv.get_reminder(rebel.user_id, self.reminder_id)

        with self.assertRaises(ex.AuthorizationError, msg="Update reminder authoriation assert"):
            self.serv.update_reminder(rebel.user_id, self.reminder_id, raise_date=datetime.now().date())

        with self.assertRaises(ex.AuthorizationError, msg="Delete reminder authorization assert"):
            self.serv.delete_reminder(rebel.user_id, self.reminder_id)

        self.serv.delete_user(rebel.user_id)

    def test_reminder_delete(self):
        self.serv.delete_reminder(self.user_id, self.reminder_id)

        with self.assertRaises(ex.ObjectIsNotFoundError, msg="Reminder deletion assert"):
            self.serv.get_reminder(self.user_id, self.reminder_id)


class NotificationTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        configuration = JSONConfigurator()

        data_storage = SQLAlchemyStorage(connection_string=configuration.connection_string)

        cls.serv = services.Service(data_storage)
        cls.ds = data_storage

    def setUp(self):
        self.user = self.serv.add_user(mo.User("creator@"))
        self.user_id = self.user.user_id

        self.task = mo.Task(task_type=me.TaskType.SIMPLE.value, user_creator=self.user_id,
                            title="test task", status=me.TaskStatus.CREATED.value)
        self.task = self.serv.add_task(self.task)

        self.date = mo.TaskDate(self.task.task_id, me.DateType.START.value, datetime.now().date())
        self.date = self.serv.add_date(self.user_id, self.date)
        self.date_id = self.date.date_id

        self.notification = mo.Notification(self.date.date_id, 10, datetime.now().time())
        self.notification = self.serv.add_notification(self.user_id, self.notification)
        self.notification_id = self.notification.notification_id

    def tearDown(self):
        self.serv.delete_user(self.user_id)

    def test_notification_execution(self):
        date_delta = relativedelta(days=2)
        curr_date = datetime.now().date()

        self.serv.update_date(self.user_id, self.date_id, date=curr_date + date_delta)

        # Check that notification executed
        self.serv.update_notification(self.user_id, self.notification_id, days_to_date=3, used=me.Used.NO.value)
        self.serv.update_ripe_notifications()

        self.notification = self.serv.get_notification(self.user_id, self.notification_id)
        self.assertEqual(self.notification.used, me.Used.YES.value, "Check execution of ripe notification assert")

        # Check that notificatin wasn't executed
        self.serv.update_notification(self.user_id, self.notification_id, days_to_date=1, used=me.Used.NO.value)
        self.serv.update_ripe_notifications()

        self.notification = self.serv.get_notification(self.user_id, self.notification_id)
        self.assertEqual(self.notification.used, me.Used.NO.value, "Check execution of raw notification assert")

    def test_notification_authorization(self):
        rebel = mo.User("rebel@")
        rebel = self.serv.add_user(rebel)

        with self.assertRaises(ex.AuthorizationError, msg="Get notification authorization assert"):
            self.serv.get_notification(rebel.user_id, self.notification_id)

        with self.assertRaises(ex.AuthorizationError, msg="Update notification authorization assert"):
            self.serv.update_notification(rebel.user_id, self.notification_id, used=me.Used.NO.value)

        with self.assertRaises(ex.AuthorizationError, msg="Delete notification authorization assert"):
            self.serv.delete_notification(rebel.user_id, self.notification_id)

        rebel_notification = mo.Notification(self.date_id, 12, datetime.now().time())
        with self.assertRaises(ex.AuthorizationError, msg="Add notification authorization assert"):
            self.serv.add_notification(rebel.user_id, rebel_notification)

        self.serv.delete_user(rebel.user_id)


if __name__ == '__main__':
    unittest.main()
